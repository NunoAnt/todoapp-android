package com.example.todoapp

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

object Utils {

    fun intToDp(intIn: Int): Int {
        return (intIn * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun dipToPx(context: Context, dipValue: Float): Int {
        val metrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics).toInt()
    }
}