package com.example.todoapp

import android.content.Intent
import android.graphics.Outline
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import com.example.todoapp.databinding.ActivityUserOnBoardingActicityBinding

class UserOnBoardingActicity : AppCompatActivity() {

    private lateinit var binding: ActivityUserOnBoardingActicityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserOnBoardingActicityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.buttonSkip.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}