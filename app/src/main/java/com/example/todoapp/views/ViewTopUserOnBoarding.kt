package com.example.todoapp.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.example.todoapp.R
import com.example.todoapp.Utils

class ViewTopUserOnBoarding: View {

    private var paint: Paint? = null
    private var path: Path? = null

    var title = "Hi there! Welcome to"
    var subTitle = "TTD"

    constructor(context: Context?) : super(context) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        paint = Paint()
        path = Path()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint?.style = Paint.Style.FILL
        paint?.color = ContextCompat.getColor(context, R.color.lightBackGround)
        paint?.isAntiAlias = true

        path?.moveTo(0.0f, 0.0f)
        path?.lineTo(0.0f, height * 0.87f)
        path?.quadTo(0.5f * width, height.toFloat(), width.toFloat(), height * 0.87f)
        path?.lineTo(width.toFloat(), 0f)
        canvas.drawPath(path!!, paint!!)

        paint?.color = Color.WHITE
        paint?.textSize = Utils.intToDp(14).toFloat()
        paint?.typeface = ResourcesCompat.getFont(context, R.font.raleway_regular)
        paint?.textAlign = Paint.Align.CENTER
        canvas.drawText(title, (width / 2f), (height * 0.55f) - Utils.intToDp(60).toFloat(), paint!!)

        paint?.color = Color.WHITE
        paint?.textSize = Utils.intToDp(34).toFloat()
        paint?.textAlign = Paint.Align.CENTER
        paint?.typeface = ResourcesCompat.getFont(context, R.font.raleway_black)
        canvas.drawText(subTitle, (width / 2f), (height * 0.55f), paint!!)
    }
}