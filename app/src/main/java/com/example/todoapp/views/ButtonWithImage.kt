package com.example.todoapp.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.example.todoapp.R
import com.example.todoapp.Utils

/**
 * ButtonWithImage.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 05/04/2024 21:59.
 * Copyright © 2024 Nuno Antunes. All rights reserved.
 */

class ButtonWithImage: View {

    private lateinit var paint: Paint
    private lateinit var paintText: Paint
    private var textBounds = Rect()

    private var icon: Drawable? = null
    private var text = "Button"
    private var fontSize = 14
    private var iconSize = 14
    private var showIcon = true

    private var isOnClick = false
    var touchEvent: (() -> Unit)? = null

    constructor(context: Context?) : super(context) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
            ) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    private fun init() {
        paint = Paint()
        setPaintText()

        icon = ContextCompat.getDrawable(context, R.drawable.ic_trash)
    }

    private fun init(attrs: AttributeSet?) {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ButtonWithImage)
        showIcon = attributes.getBoolean(R.styleable.ButtonWithImage_showIcon, true)
        text = attributes.getString(R.styleable.ButtonWithImage_text) ?: "Button"

        paint = Paint()
        setPaintText()

        icon = ContextCompat.getDrawable(context, R.drawable.ic_trash)
    }

    private fun setPaintText() {
        paintText = Paint()
        paintText.style = Paint.Style.FILL_AND_STROKE
        paintText.color = ContextCompat.getColor(context, R.color.white)
        paintText.textSize = Utils.intToDp(fontSize).toFloat()
        paintText.textAlign = Paint.Align.CENTER
        paintText.typeface = ResourcesCompat.getFont(context, R.font.raleway_bold)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWith = paintText.measureText(text, 0, text.length).toInt() + Utils.intToDp(50)
        val desiredHeight = Utils.intToDp(fontSize) + Utils.intToDp(16)

        val widthMeasureMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthMeasureSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMeasureMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightMeasureSize = MeasureSpec.getSize(heightMeasureSpec)

        //Measure Width
        val width = when (widthMeasureMode) {
            MeasureSpec.EXACTLY -> {
                //Must be this size
                widthMeasureSize
            }
            MeasureSpec.AT_MOST -> {
                //Can't be bigger than...
                Math.min(desiredWith, heightMeasureSize)
            }
            else -> {
                //Be whatever you want
                desiredWith
            }
        }

        val height = when (heightMeasureMode) {
            MeasureSpec.EXACTLY -> {
                heightMeasureSize
            }
            MeasureSpec.AT_MOST -> {
                Math.min(desiredHeight, heightMeasureSize)
            }
            else -> {
                desiredHeight
            }
        }

        setMeasuredDimension(width, height)
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = ContextCompat.getColor(context, R.color.color_red_delete)
        paint.isAntiAlias = true
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)

        paintText.getTextBounds(text, 0, text.length, textBounds)
        canvas.drawText(text, width.toFloat() / 2, (height.toFloat() + Utils.intToDp(fontSize / 2)) / 2, paintText)

        if (showIcon) {
            icon?.let {
                val startWidthPositionIcon = (width.toFloat() / 2) - textBounds.width() / 2 - (Utils.intToDp(iconSize))
                val endWithPositionIcon = startWidthPositionIcon + Utils.intToDp(iconSize)
                val startHeightPositionIcon = ((height.toFloat() + Utils.intToDp(fontSize)) / 2) - Utils.intToDp(iconSize)
                val endHeightPositionIcon = startHeightPositionIcon + Utils.intToDp(iconSize)
                it.setBounds(startWidthPositionIcon.toInt(), startHeightPositionIcon.toInt(), endWithPositionIcon.toInt(), endHeightPositionIcon.toInt())
                it.draw(canvas)
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                if (!isOnClick) {
                    isOnClick = true
                    touchEvent?.invoke()
                }
            }
            MotionEvent.ACTION_UP -> {
                isOnClick = false
            }
        }
        return true
    }
}