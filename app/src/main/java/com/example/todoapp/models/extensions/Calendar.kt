package com.example.todoapp.models.extensions

import java.util.Calendar
import java.util.Locale

/**
 * Return the month of the calendar
 *
 * @return month Ex: January, March
 */
val Calendar.monthText: String
    get() = this.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())?.toString() ?: ""

/**
 * Return the hour of the calendar
 *
 * @return month Ex: 1, 2, 3..12
 */
val Calendar.hour: Int
    get() = this.get(Calendar.HOUR_OF_DAY)

/**
 * Return the minutes of the calendar
 *
 * @return month Ex: 1, 2, 3..12
 */
val Calendar.minutes: Int
    get() = this.get(Calendar.MINUTE)

/**
 * Return the day of the calendar
 *
 * @return month Ex: 1, 2, 3..31
 */
val Calendar.day: Int
    get() = this.get(Calendar.DAY_OF_MONTH)

/**
 * Return the month of the calendar
 *
 * @return month Ex: 1, 2, 3..31
 */
val Calendar.month: Int
    get() = this.get(Calendar.MONTH)

/**
 * Return the year of the calendar
 *
 * @return month Ex: 1, 2, 3..31
 */
val Calendar.year: Int
    get() = this.get(Calendar.YEAR)


/**
 * Get the total days of the month
 *
 * @return days of the current month
 */
val Calendar.daysOfMonth: Int
    get() = this.getActualMaximum(Calendar.DAY_OF_MONTH)


/**
 * Get the week day of the first day of the current month
 *
 * @return number of week in Int EX: 1 = Sunday Ex: 2 = Monday
 */
val Calendar.dayOnWeekOfTheFirstDayOfTheMonth: Int
    get() {
        this.set(Calendar.DAY_OF_MONTH, 1)
        return this.get(Calendar.DAY_OF_WEEK)
    }
