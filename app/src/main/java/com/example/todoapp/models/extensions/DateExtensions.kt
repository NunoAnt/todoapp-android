package com.example.todoapp.models.extensions

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

/**
 * DateExtensions.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 11/03/2024 22:29.
 * Copyright © 2024 Mindprober. All rights reserved.
 */

/**
 * Return the day of the date
 *
 * @return month Ex: 1, 2, 3..31
 */
val Date.getDay: Int
    get() {
        val calendar = Calendar.getInstance()
        calendar.time = this
        return calendar.day
    }

/**
 * Return the true if the date are the same in year, month, day
 *
 * @return if (year == year && month == month && day == day) true else false
 */
fun Date.compare(anotherDate: Date): Boolean {
    val calendar = Calendar.getInstance()
    val anotherCalendar = Calendar.getInstance()

    calendar.time = this
    anotherCalendar.time = anotherDate

    if (calendar.year == anotherCalendar.year
        && calendar.month == anotherCalendar.month
        && calendar.day == anotherCalendar.day)
        return true
    else
        return false
}

/**
 * Return the hour and mins of the data
 *
 * @return Ex: 12:35
 */
fun Date.toHourString(): String {
    val patternDate = "kk:mm"
    val simpleDatFormat = SimpleDateFormat(patternDate, Locale.getDefault())
    return simpleDatFormat.format(this)
}

/**
 * Return the day and the month of the data
 *
 * @return Ex: 12 May
 */
fun Date.toDayMonth(): String {
    val patternDate = "dd MMM"
    val simpleDatFormat = SimpleDateFormat(patternDate, Locale.getDefault())
    return simpleDatFormat.format(this)
}

