package com.example.todoapp.models

import java.util.Date

/**
 * CalendarElement.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 10/03/2024 04:14.
 * Copyright © 2024 Mindprober. All rights reserved.
 */

data class CalendarElement(
    var date: Date?,
    var task: Task?
)