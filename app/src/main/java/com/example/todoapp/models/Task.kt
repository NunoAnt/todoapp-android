package com.example.todoapp.models

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.PrimaryKey
import androidx.room.Query
import org.json.JSONException
import org.json.JSONObject

@Entity
data class Task(
    @PrimaryKey val uid: String,
    @ColumnInfo(name = "topic") val topic: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "date") val date: Long,
    @ColumnInfo(name = "color") val color: String,
    @ColumnInfo(name = "done") var done: Boolean = false
) {
    fun toJson(): JSONObject {
        val jsonObject = JSONObject()
        jsonObject.put("uid", uid)
        jsonObject.put("topic", topic)
        jsonObject.put("description", description)
        jsonObject.put("date", date)
        jsonObject.put("color", color)
        jsonObject.put("done", done)

        return jsonObject
    }

    companion object {
        fun fromJson(jsonObject: JSONObject): Task? {
            return try {
                val uid = jsonObject.getString("uid")
                val topic = jsonObject.getString("topic")
                val description = jsonObject.getString("description")
                val date = jsonObject.getLong("date")
                val color = jsonObject.getString("color")
                val done = jsonObject.getBoolean("done")

                Task(uid, topic, description, date, color, done)
            } catch (e: JSONException) {
                null
            }
        }
    }
}

@Dao
interface TaskDao {
    @Query("SELECT * FROM task")
    fun getAll(): List<Task>

    @Query("UPDATE task SET done = :done WHERE uid = :taskId")
    fun updateTaskToDoneById(taskId: String, done: Boolean)

    @Insert
    fun insertAll(vararg task: Task)

    @Delete
    fun delete(task: Task)
}