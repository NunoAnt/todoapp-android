package com.example.todoapp.ui.tasks

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.AppDatabase
import com.example.todoapp.models.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * TaskListViewModel.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 18/03/2024 22:09.
 * Copyright © 2024 Mindprober. All rights reserved.
 */

data class TaskListUiState(
    val taskList: List<Task> = ArrayList()
)


class TaskListViewModel(application: Application): AndroidViewModel(application) {

    private val _uiState = MutableStateFlow(TaskListUiState())
    val uiState: StateFlow<TaskListUiState> = _uiState.asStateFlow()

    fun getAllTask() {
        val db = AppDatabase.getDatabase(getApplication())

        viewModelScope.launch(Dispatchers.IO) {
            val listTask = db?.taskDao()?.getAll()

            _uiState.update { currentState ->
                currentState.copy(
                    taskList = listTask?.toList()?.sortedWith(compareBy { it.date }) ?: ArrayList(),
                )
            }
        }
    }
}