package com.example.todoapp.ui.deleteTask

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.AppDatabase
import com.example.todoapp.models.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * TaskDialogViewModel.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 20/04/2024 02:47.
 * Copyright © 2024 Mindprober. All rights reserved.
 */

data class TaskDialogUiState(
    val taskDone: Boolean = false
)

class TaskDialogViewModel(application: Application): AndroidViewModel(application) {

    private val _uiState = MutableStateFlow(TaskDialogUiState())
    val uiState: StateFlow<TaskDialogUiState> = _uiState.asStateFlow()

    fun updateTaskCompleted(task: Task) {
        val db = AppDatabase.getDatabase(getApplication())

        task.done = !task.done

        viewModelScope.launch(Dispatchers.IO) {
            db?.taskDao()?.updateTaskToDoneById(task.uid, task.done)
        }

        _uiState.update { currentState ->
            currentState.copy(
                taskDone = task.done
            )
        }
    }

    fun deleteTask(task: Task) {
        val db = AppDatabase.getDatabase(getApplication())

        viewModelScope.launch(Dispatchers.IO) {
            db?.taskDao()?.delete(task)
        }
    }
}