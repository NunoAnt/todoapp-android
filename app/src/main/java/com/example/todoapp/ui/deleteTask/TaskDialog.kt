package com.example.todoapp.ui.deleteTask

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.todoapp.R
import com.example.todoapp.databinding.DialogTaskBinding
import com.example.todoapp.models.Task
import com.example.todoapp.models.extensions.toDayMonth
import com.example.todoapp.models.extensions.toHourString
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.util.Date

/**
 * TaskDialog.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 06/04/2024 02:38.
 * Copyright © 2024 Nuno Antunes. All rights reserved.
 */

class TaskDialog: DialogFragment() {

    private var _binding: DialogTaskBinding? = null
    private val binding get() = _binding!!

    private var onDismissListener: (() -> Unit)? = null
    fun setOnDismissListener(listener: () -> Unit) {
        onDismissListener = listener
    }

    private var task: Task? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireContext())
        _binding = DialogTaskBinding.inflate(this.layoutInflater)
        val dialogView: View = binding.root

        arguments?.let {
            val taskJson = it.getString(TASK_OBJECT)
            if (taskJson != null) {
                task = Task.fromJson(JSONObject(taskJson))
            }
        }

        builder.setView(dialogView)

        val alertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val viewModel: TaskDialogViewModel by viewModels()
        lifecycleScope.launch {
            viewModel.uiState.collect { state ->
                if (state.taskDone)
                    binding.imageViewDone.setImageResource(R.drawable.ic_selected)
                else
                    binding.imageViewDone.setImageResource(R.drawable.ic_unselected)
            }
        }

        task?.let {
            val taskDate = Date(it.date)
            binding.textViewTaskTitle.text = it.topic
            binding.textViewTaskDescription.text = it.description
            binding.textViewTaskHour.text = taskDate.toHourString()
            binding.textViewTaskDayMonth.text = taskDate.toDayMonth()
            if (it.done)
                binding.imageViewDone.setImageResource(R.drawable.ic_selected)
            else
                binding.imageViewDone.setImageResource(R.drawable.ic_unselected)
        }

        binding.imageViewDone.setOnClickListener {
            task?.let { viewModel.updateTaskCompleted(it) }
        }

        binding.buttonDelete.touchEvent = {
            task?.let { viewModel.deleteTask(it) }
            dismiss()
        }

        return alertDialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }

    companion object {
        private const val TASK_OBJECT = "task"

        fun newInstance(task: Task): TaskDialog {
            val dialog = TaskDialog()
            val bundle = Bundle()
            bundle.putString(TASK_OBJECT, task.toJson().toString())
            dialog.arguments = bundle
            return dialog
        }
    }
}