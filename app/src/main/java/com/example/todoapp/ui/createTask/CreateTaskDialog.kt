package com.example.todoapp.ui.createTask

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.TimePicker
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.databinding.DialogCreateTaskBinding
import com.example.todoapp.models.extensions.day
import com.example.todoapp.models.extensions.minutes
import com.example.todoapp.models.extensions.month
import com.example.todoapp.models.extensions.year
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar


class CreateTaskDialog: DialogFragment() {

    private var _binding: DialogCreateTaskBinding? = null
    private val binding get() = _binding!!

    private var onDismissListener: (() -> Unit)? = null
    fun setOnDismissListener(listener: () -> Unit) {
        onDismissListener = listener
    }

    private var createTaskViewModel: CreateTaskViewModel? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireContext())
        _binding = DialogCreateTaskBinding.inflate(this.layoutInflater)
        val dialogView: View = binding.root

        builder.setView(dialogView)

        val alertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val viewModel: CreateTaskViewModel by viewModels()
        createTaskViewModel = viewModel

        binding.buttonCreateTask.setOnClickListener {
            val year = binding.editTextDateYear.text.toString().toInt()
            val month = binding.editTextDateMonth.text.toString().toInt()
            val day = binding.editTextDateDay.text.toString().toInt()
            val hours = binding.editTextHour.text.toString().toInt()
            val minutes = binding.editTextMinutes.text.toString().toInt()

            val date = GregorianCalendar(year, month, day, hours, minutes).time

            viewModel.createTask(
                binding.editTextTopic.text.toString(),
                binding.editTextWriteDescription.text.toString(),
                date,
                viewModel.selectedColor ?: CreateTaskViewModel.ORANGE_COLOR
            )

            dismiss()
        }

        binding.viewDate.setOnClickListener {
            val calendar: Calendar = GregorianCalendar()
            calendar.time = Date()

            DatePickerDialog(
                this@CreateTaskDialog.requireContext(),
                R.style.DatePickerDialog,
                object : OnDateSetListener {
                    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
                        binding.editTextDateDay.setText(day.toString())
                        binding.editTextDateMonth.setText(month.toString())
                        binding.editTextDateYear.setText(year.toString())
                    }
                },
                calendar.year,
                calendar.month,
                calendar.day
            ).show()
        }

        binding.viewHour.setOnClickListener {
            val calendar: Calendar = GregorianCalendar()
            calendar.time = Date()

            TimePickerDialog(
                this@CreateTaskDialog.requireContext(),
                object : TimePickerDialog.OnTimeSetListener {
                    override fun onTimeSet(view: TimePicker?, hour: Int, minutes: Int) {
                        binding.editTextHour.setText(hour.toString())
                        binding.editTextMinutes.setText(minutes.toString())
                    }
                },
                calendar.month,
                calendar.minutes,
                true
            ).show()
        }

        val adapter = ColorSelectorAdapter()
        adapter.onColorSelect = { color ->
            when (color) {
                CreateTaskViewModel.ORANGE_COLOR -> {
                    binding.imageViewChooseColor.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_orange))
                }
                CreateTaskViewModel.BLUE_COLOR -> {
                    binding.imageViewChooseColor.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_blue))
                }
                CreateTaskViewModel.BABY_BLUE -> {
                    binding.imageViewChooseColor.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_light_blue))
                }
                else -> {
                    binding.imageViewChooseColor.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_orange))
                }
            }

            binding.recyclerViewColorChooser.visibility = RecyclerView.GONE

            viewModel.selectedColor = color
        }
        binding.recyclerViewColorChooser.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewColorChooser.itemAnimator = DefaultItemAnimator()
        binding.recyclerViewColorChooser.adapter = adapter

        binding.imageViewChooseColor.setOnClickListener {
            if (binding.recyclerViewColorChooser.isVisible)
                binding.recyclerViewColorChooser.visibility = RecyclerView.INVISIBLE
            else
                binding.recyclerViewColorChooser.visibility = RecyclerView.VISIBLE
        }

        return alertDialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }


    val listColors = listOf(
        CreateTaskViewModel.ORANGE_COLOR,
        CreateTaskViewModel.BLUE_COLOR,
        CreateTaskViewModel.BABY_BLUE
    )
    inner class ColorSelectorAdapter: RecyclerView.Adapter<ColorSelectorAdapter.ViewHolder>() {

        var onColorSelect: ((color: String) -> Unit)? = null

        inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
            val imageViewColorChooser = itemView.findViewById<ImageView>(R.id.imageViewChooseColor)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_color_choser, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            when (listColors[position]) {
                CreateTaskViewModel.ORANGE_COLOR -> {
                    holder.imageViewColorChooser.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_orange))
                }
                CreateTaskViewModel.BLUE_COLOR -> {
                    holder.imageViewColorChooser.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_blue))
                }
                CreateTaskViewModel.BABY_BLUE -> {
                    holder.imageViewColorChooser.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_light_blue))
                }
                else -> {
                    holder.imageViewColorChooser.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_orange))
                }
            }

            holder.imageViewColorChooser.setOnClickListener {
                onColorSelect?.invoke(listColors[position])
            }
        }

        override fun getItemCount(): Int {
            return listColors.size
        }
    }

    companion object {
        fun newInstance(): CreateTaskDialog {
            return CreateTaskDialog()
        }
    }
}