package com.example.todoapp.ui.createTask

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.AppDatabase
import com.example.todoapp.models.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Date
import java.util.UUID

/**
 * CreateTaskViewModel.kt
 * ToDoApp
 *
 * Created by Nuno Antunes on 26/02/2024 18:39.
 * Copyright © 2024 Mindprober. All rights reserved.
 */

class CreateTaskViewModel(application: Application): AndroidViewModel(application) {

    var selectedColor: String? = null

    fun createTask(topic: String, description: String, date: Date, selectedColor: String) {
        val db = AppDatabase.getDatabase(getApplication())

        if (topic.isNotEmpty()
            && description.isNotEmpty()) {

            val task = Task(
                UUID.randomUUID().toString(),
                topic,
                description,
                date.time,
                selectedColor,
                false
            )

            viewModelScope.launch(Dispatchers.IO) {
                db?.taskDao()?.insertAll(task)
            }
        } else {
            // please fill all answers
        }
    }

    companion object {
        const val ORANGE_COLOR = "orange_color"
        const val BLUE_COLOR = "blue_color"
        const val BABY_BLUE = "baby_blue"
    }
}