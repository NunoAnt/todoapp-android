package com.example.todoapp.ui.more

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.AppDatabase
import com.example.todoapp.models.CalendarElement
import com.example.todoapp.models.Task
import com.example.todoapp.models.extensions.compare
import com.example.todoapp.models.extensions.dayOnWeekOfTheFirstDayOfTheMonth
import com.example.todoapp.models.extensions.daysOfMonth
import com.example.todoapp.models.extensions.month
import com.example.todoapp.models.extensions.monthText
import com.example.todoapp.models.extensions.year
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import java.util.Locale

data class MoreUiState(
    val dayList: List<CalendarElement> = ArrayList(),
    val month: String = "",
    val taskList: List<Task> = ArrayList(),
    val viewMonth: Boolean = true
)

class MoreViewModel(application: Application): AndroidViewModel(application) {

    private val _uiState = MutableStateFlow(MoreUiState())
    val uiState: StateFlow<MoreUiState> = _uiState.asStateFlow()

    fun setMonthAndDays() {
        val calendarElements: MutableList<CalendarElement> = ArrayList()

        // check the week day when month started and add empty elements to list
        val cal = Calendar.getInstance(Locale.getDefault())
        val numberOfWhiteSpacesToAdd = cal.dayOnWeekOfTheFirstDayOfTheMonth // number of week in Int EX: 1 = Sunday Ex: 2 = Monday
        val numberOfDaysInThisMonth = cal.daysOfMonth

        // set month in the header of the calendar
        val month = cal.monthText.replaceFirstChar { it.uppercase() }
        _uiState.update { currentState ->
            currentState.copy(
                month = month
            )
        }

        // add days only if not start on Sunday
        if (numberOfWhiteSpacesToAdd != 1) {
            for (i in 1 until numberOfWhiteSpacesToAdd) { // until = small or equal
                calendarElements.add(CalendarElement(null, null))
            }
        }

        for (i in 1..numberOfDaysInThisMonth) {
            calendarElements.add(CalendarElement(GregorianCalendar(cal.year, cal.month, i).time, null))
        }

        _uiState.update { currentState ->
            currentState.copy(
                dayList = calendarElements
            )
        }
    }

    fun getTasksOfCurrentDay() {
        val db = AppDatabase.getDatabase(getApplication())
        val currentDate = Date()

        viewModelScope.launch(Dispatchers.IO) {
            val listTask = db?.taskDao()?.getAll()
            val currentDayTaskList: MutableList<Task> = ArrayList()

            listTask?.forEach { task ->
                val taskDate = Date(task.date)
                if (taskDate.compare(currentDate)) {
                    currentDayTaskList.add(task)
                }
            }

            _uiState.update { currentState ->
                currentState.copy(
                    taskList = currentDayTaskList.toList().sortedWith(compareBy { it.date }),
                    viewMonth = false
                )
            }
        }
    }

    fun getTasksOfCurrentMonth() {
        val db = AppDatabase.getDatabase(getApplication())
        val currentDate = Date()

        viewModelScope.launch(Dispatchers.IO) {

            val newDayList = _uiState.value.dayList
            val listTask = db?.taskDao()?.getAll()
            val monthTaskList: MutableList<Task> = ArrayList()

            // remove all tasks from calendar
            newDayList.forEach {
                it.task = null
            }

            // get values for that month
            listTask?.forEach { task ->
                val taskDate = Date(task.date)
                if (taskDate.month == currentDate.month && taskDate.year == currentDate.year) {
                    monthTaskList.add(task)
                }
            }

            // update if new tasks in calendar
            listTask?.forEach {
                val taskDate = Date(it.date)

                for (index in newDayList.indices) {
                    if (newDayList[index].date != null && taskDate.compare(newDayList[index].date!!)) {
                        newDayList[index].task = it
                    }
                }
            }

            _uiState.update { currentState ->
                currentState.copy(
                    dayList = newDayList.toList(),
                    taskList = monthTaskList.toList().sortedWith(compareBy { it.date }),
                    viewMonth = true
                )
            }
        }
    }
}