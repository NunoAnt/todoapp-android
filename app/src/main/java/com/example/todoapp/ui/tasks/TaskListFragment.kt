package com.example.todoapp.ui.tasks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todoapp.databinding.FragmentTaskListBinding
import com.example.todoapp.ui.adapters.TaskAdapter
import com.example.todoapp.ui.adapters.VerticalSpaceItemDecoration
import com.example.todoapp.ui.deleteTask.TaskDialog
import kotlinx.coroutines.launch


class TaskListFragment : Fragment() {

    private var _binding: FragmentTaskListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentTaskListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel: TaskListViewModel by viewModels()

        val verticalSpaceItemDecoration = VerticalSpaceItemDecoration(16)
        val taskAdapter = TaskAdapter(viewModel?.uiState?.value?.taskList ?: ArrayList())
        taskAdapter.onItemClicked = {
            val dialog = TaskDialog.newInstance(it)
            dialog.show(childFragmentManager, "dialog_task")
            dialog.setOnDismissListener {
                viewModel.getAllTask()
            }
        }

        binding.recyclerViewEvents.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewEvents.itemAnimator = DefaultItemAnimator()
        binding.recyclerViewEvents.adapter = taskAdapter
        binding.recyclerViewEvents.addItemDecoration(verticalSpaceItemDecoration)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel?.let { viewModel ->
                viewModel.uiState
                    .flowWithLifecycle(this@TaskListFragment.lifecycle, Lifecycle.State.STARTED)
                    .collect { state ->
                        taskAdapter.tasks = state.taskList
                        taskAdapter.notifyDataSetChanged()
                    }
            }
        }

        viewModel.getAllTask()
    }
}