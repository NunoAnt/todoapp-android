package com.example.todoapp.ui.more

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.databinding.FragmentMoreBinding
import com.example.todoapp.models.extensions.getDay
import com.example.todoapp.ui.adapters.TaskAdapter
import com.example.todoapp.ui.adapters.VerticalSpaceItemDecoration
import com.example.todoapp.ui.createTask.CreateTaskViewModel
import com.example.todoapp.ui.deleteTask.TaskDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.coroutines.launch

class MoreFragment : Fragment() {

    private var _binding: FragmentMoreBinding? = null
    private val binding get() = _binding!!

    private var moreViewModel: MoreViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentMoreBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tabLayout.getTabAt(0)?.select()
        val tabView = binding.tabLayout.getTabAt(0)?.view as ViewGroup
        tabView.requestLayout()
        ViewCompat.setBackground(tabView, ContextCompat.getDrawable(requireContext(), R.drawable.tab_bg_selected))

        binding.tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    moreViewModel?.getTasksOfCurrentMonth()
                } else {
                    moreViewModel?.getTasksOfCurrentDay()
                }

                tab.view.requestLayout()
                ViewCompat.setBackground(tab.view, ContextCompat.getDrawable(requireContext(), R.drawable.tab_bg_selected))
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                tab.view.requestLayout()
                ViewCompat.setBackground(tab.view, ContextCompat.getDrawable(requireContext(), R.drawable.tab_bg_normal))
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                tab.view.requestLayout()
                ViewCompat.setBackground(tab.view, ContextCompat.getDrawable(requireContext(), R.drawable.tab_bg_selected))
            }
        })

        val mLayoutManager = GridLayoutManager(requireContext(), 7)
        mLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                if (binding.recyclerViewCalendar.adapter?.getItemViewType(position) == VIEW_TYPE_HEADER) {
                    return 1
                } else {
                    return 7
                }
            }
        }

        val viewModel: MoreViewModel by viewModels()
        moreViewModel = viewModel

        val headerAdapter = HeaderAdapter()
        val calendarAdapter = CalendarAdapter()
        binding.recyclerViewCalendar.layoutManager = mLayoutManager
        binding.recyclerViewCalendar.itemAnimator = DefaultItemAnimator()
        binding.recyclerViewCalendar.adapter = ConcatAdapter(headerAdapter, calendarAdapter)

        val verticalSpaceItemDecoration = VerticalSpaceItemDecoration(16)
        val taskAdapter = TaskAdapter(moreViewModel?.uiState?.value?.taskList ?: ArrayList())
        taskAdapter.onItemClicked = {
            val dialog = TaskDialog.newInstance(it)
            dialog.show(childFragmentManager, "dialog_task")
            dialog.setOnDismissListener {
                if (moreViewModel?.uiState?.value?.viewMonth == true)
                    moreViewModel?.getTasksOfCurrentMonth()
                else
                    moreViewModel?.getTasksOfCurrentDay()
            }
        }

        binding.recyclerViewEvents.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewEvents.itemAnimator = DefaultItemAnimator()
        binding.recyclerViewEvents.adapter = taskAdapter
        binding.recyclerViewEvents.addItemDecoration(verticalSpaceItemDecoration)

        viewLifecycleOwner.lifecycleScope.launch {
            moreViewModel?.let { viewModel ->
                viewModel.uiState
                    .flowWithLifecycle(this@MoreFragment.lifecycle, Lifecycle.State.STARTED)
                    .collect { state ->
                        if (state.viewMonth) {
                            if (state.dayList.isNotEmpty()) {
                                calendarAdapter.notifyDataSetChanged()
                            }

                            binding.recyclerViewCalendar.visibility = RecyclerView.VISIBLE
                        } else {
                            binding.recyclerViewCalendar.visibility = RecyclerView.GONE
                        }


                        taskAdapter.tasks = state.taskList
                        taskAdapter.notifyDataSetChanged()

                        binding.textViewMonth.text = state.month
                    }
            }
        }
        moreViewModel?.setMonthAndDays()
        moreViewModel?.getTasksOfCurrentMonth()
    }

    fun updateData() {
        if (moreViewModel?.uiState?.value?.viewMonth == true)
            moreViewModel?.getTasksOfCurrentMonth()
        else
            moreViewModel?.getTasksOfCurrentDay()
    }

    inner class CalendarAdapter: RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {
        inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
            val dayTextView = itemView.findViewById<TextView>(R.id.textViewDay)
            val viewLabelEvent = itemView.findViewById<ImageView>(R.id.imageViewChooseColor)
        }

        override fun getItemViewType(position: Int): Int {
            return VIEW_TYPE_CALENDAR
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_days_calendar, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val dayList = moreViewModel?.uiState?.value?.dayList?.get(position)
            dayList?.let {
                if (it.date != null) {
                    holder.dayTextView.text = it.date!!.getDay.toString()

                    if (it.task != null) {
                        holder.viewLabelEvent.visibility = View.VISIBLE

                        when (dayList.task?.color) {
                            CreateTaskViewModel.ORANGE_COLOR -> {
                                holder.viewLabelEvent.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_orange))
                            }
                            CreateTaskViewModel.BLUE_COLOR -> {
                                holder.viewLabelEvent.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_blue))
                            }
                            CreateTaskViewModel.BABY_BLUE -> {
                                holder.viewLabelEvent.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_light_blue))
                            }
                            else -> {
                                holder.viewLabelEvent.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.color_picker_orange))
                            }
                        }
                    } else
                        holder.viewLabelEvent.visibility = View.GONE
                } else {
                    holder.dayTextView.visibility = View.GONE
                    holder.viewLabelEvent.visibility = View.GONE
                }
            }
        }

        override fun getItemCount(): Int {
            return moreViewModel?.uiState?.value?.dayList?.size ?: 0
        }
    }

    inner class HeaderAdapter: RecyclerView.Adapter<HeaderAdapter.HeaderViewHolder>() {
        inner class HeaderViewHolder(view: View): RecyclerView.ViewHolder(view) {
            private val monthTextView = itemView.findViewById<TextView>(R.id.textViewMonth)

            fun bind(month: String) { monthTextView.text = month }
        }

        override fun getItemViewType(position: Int): Int {
            return VIEW_TYPE_HEADER
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeaderViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.header_calendar, parent, false)
            return HeaderViewHolder(view)
        }

        override fun onBindViewHolder(holder: HeaderViewHolder, position: Int) {
            holder.bind(moreViewModel?.uiState?.value?.month ?: "")
        }

        override fun getItemCount(): Int {
            return 1
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val VIEW_TYPE_HEADER = 1
        private const val VIEW_TYPE_CALENDAR = 2
    }
}