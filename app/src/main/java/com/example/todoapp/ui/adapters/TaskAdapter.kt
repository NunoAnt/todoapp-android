package com.example.todoapp.ui.adapters

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.example.todoapp.R
import com.example.todoapp.models.Task
import com.example.todoapp.models.extensions.toDayMonth
import com.example.todoapp.models.extensions.toHourString
import java.util.Date


class TaskAdapter(tasks: List<Task>): RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    var tasks: List<Task>
    var onItemClicked: ((Task) -> Unit)? = null

    init {
        this.tasks = tasks
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val eventNameTextView = itemView.findViewById<TextView>(R.id.textViewTaskName)
        val checkImageView = itemView.findViewById<ImageView>(R.id.imageViewTaskChecked)
        val hourTextView = itemView.findViewById<TextView>(R.id.textViewTaskHour)
        val dayTextView = itemView.findViewById<TextView>(R.id.textViewTaskDayMonth)
        val linearLayoutTask = itemView.findViewById<LinearLayout>(R.id.linearLayoutTask)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_task, parent, false)
        view.isClickable = true
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]

        holder.eventNameTextView.text = task.topic

        val date = Date(task.date)
        holder.hourTextView.text = date.toHourString()
        holder.dayTextView.text = date.toDayMonth()
        if (task.done)
            holder.checkImageView.setImageResource(R.drawable.ic_selected)
        else
            holder.checkImageView.setImageResource(R.drawable.ic_unselected)

        holder.linearLayoutTask.setOnClickListener {
            onItemClicked?.invoke(task)
        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }
}


class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int) : ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = verticalSpaceHeight
    }
}