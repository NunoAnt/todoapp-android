package com.example.todoapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.todoapp.databinding.ActivityMainBinding
import com.example.todoapp.ui.createTask.CreateTaskDialog
import com.example.todoapp.ui.more.MoreFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_more, R.id.navigation_all_task_list, R.id.navigation_notifications
            )
        )
        //setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        binding.floatingActionButtonCreateTask.bringToFront()
        binding.floatingActionButtonCreateTask.setOnClickListener {
            val dialog = CreateTaskDialog.newInstance()
            dialog.show(supportFragmentManager, "dialog_create_task")
            dialog.setOnDismissListener {
                val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main)
                val fragment = navHostFragment?.childFragmentManager?.fragments?.get(0)
                if (fragment is MoreFragment) {
                    fragment.updateData()
                }
            }
        }
    }
}